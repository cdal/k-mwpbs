# K-MWPBS (Top-K Maximum Weight Pathway Constrained Bipartite Subgraph)
--------------------------------
*Prerequisite*
--------------------------------
Please install:

python >= 3.6 version

Gurobi version 9

networkx version 2.4


Command:
------------------------------
python KMWPBS.py uniq_gene_list discover_me_gene_pairs kegg_pathway.gmt kwbps_plot_res > kwbps_results

Input parameters:

uniq_gene_list: file contains unique gene list found in discover_me_gene_pairs

discover_me_gene_pairs: discover mutual exclusivity gene pairs with third column as p-value (cut-off < 0.05)

kegg_pathway.gmt: kegg pathway

kwbps_plot_res: output kmwbps plot name

kwbps_results: kwbps print out results, bipartite graph kwbps results

Output:

.png plot (with kwbps_plot_res name)
kwbps bipartite results


Simulation Command:
--------------------------------
python simulate01.py n m p k

Input parameters:

n (int) – The number of nodes in the first bipartite set

m (int) – The number of nodes in the second bipartite set

p (float) – Probability for edge creation, i.e. 0.5

k (int) - The number of random edges to be added

